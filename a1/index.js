// console.log("Hello World");

	let firstName = "MV";
	console.log("First Name: " + firstName);

	let lastName = "De Leon";
	console.log("Last Name " + lastName);

	let age = 35;
	console.log("Age: " + age);

	let hobbies = ["Binge Watching", "Playing Mobile Games", "Sleeping"];
	console.log("Hobbies: ")
	console.log(hobbies);

	let workAddress = {

		houseNumber: 381,
		street: "Dela Virgen St",
		city: "Antipolo City",
		state: "Rizal"

	}
	console.log("Work Address:");
	console.log(workAddress);

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);

	
	
	let friends = ["Tony", "Bruce", "Thor","Natasha" ,"Clint" ,"Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);