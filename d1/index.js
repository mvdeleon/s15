console.log("Hello World");

// JavaScript renders web pages in an interactive and dynamic fashion. Meaning, it enables us to create dynamically updating content, control multimedia, and animate image

// Syntax, Statements, and Comments

// Statements:

// Statements in programming are instructions that we tell the computer to perform
// JS Statements usually end with a semicolon (;)
// Semicolns are not required in JS, but we will use it to help us train to locate where a statement ends

// Syntax in programming, it is the set of rules that describes how statements must be constructed

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code


/*There are two types of comments:

1. The single-line comment (ctrl + /)
	**denoted by two slashes

2. The multi-line comment (ctrl + shift + /)
	** denoted by a slash and asterisk

*/

// Variables

// It is used to contain data
// This makes it easier for us to associate information stored in our devices to actual "names" about information

// Declaring variables
// declaring variables - tells our devices that a variable name is created and is ready to store data

let myVariable;
console.log(myVariable);

// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

// Syntax
	// let/ const variableName;

// console.log() is useful for printing values of variables or certain results of code into Google Chrome browser's console
// constant

// let hello;
// Console.log(hello);


let productName = "Desktop Computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
// const pi = 3.1416;

productName = 'Laptop';
console.log(productName);

// console.log(interest);

// let friend = 'Kate';
// friend = 'Nej';
// console.log(friend);

// let friend = "Kate";
// let friend = "Nej"; //error
// console.log(friend);

// interest = 4.489;

let supplier;
supplier = "John Smith Tradings";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// const pi;
// pi = 3.1416;
// console.log(pi);

// a = 5;
// console.log(a);
// var a;

// a = 5;
// console.log(a);
// let a;

let outerVariable = "Hello";

{
	let innerVariable = "Hello again";
}

console.log(outerVariable);
// console.log(innerVariable);

// let productCode = 'DC017', productBrand = "Dell";
let productCode = 'DC017';
const productBrand = 'Dell';
console.log(productCode, productBrand);

// const let = "hello";
// console.log(let);

let country = 'Philippines';
let province = 'Metro Manila';

let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade);

let Grade = 97 / 7;
console.log(Grade);

let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);

let isMarried = false;
let inGoodConduct = true;
console.log("is Married: " + isMarried);
console.log("is Good Conduct: " + inGoodConduct);

let grades = [98.7, 92.1, 90.2, 94.6]
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

let person = {
	fullName: 'Edward Scissorhands',
	age: 25,
	isMarried: false,
	contact:["+639171234567", "8123 4567"],
	address:{
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}
console.log(myGrades);

console.log(typeof myGrades);
console.log(typeof grades);

const anime = ['OP', 'OPM', 'AOT', 'BNHA'];
anime[0] = 'JJK';

console.log(anime);

let spouse = null;
console.log(spouse);

let myNumber = 0;
let myString = '';
console.log(myNumber);
console.log(myString);

let fullName;
console.log(fullName);